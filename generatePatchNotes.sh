#!/bin/bash
if [ $# -eq 0 ]
  then
    latestVersion=`git describe --tags --abbrev=0`
    echo "No arguments supplied, generating from version $latestVersion"

else
	latestVersion=$1
fi
mkdir -p Changelogs
git --no-pager log $latestVersion..HEAD --format=%B --grep=Merge --invert-grep > ./Changelogs/Changelog-From-$latestVersion.txt
./Changelogs/PatchNotesCleaner.exe ./Changelogs/Changelog-From-$latestVersion.txt