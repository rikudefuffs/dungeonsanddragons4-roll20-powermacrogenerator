### 2.0.1

(Note: these patch notes were automatically generated from commit messages)

## Changes
* [UI] The application window can now be resized when in windowed mode
* [UI] Language selection is now made with a shortcut instead of just up/down key, to prevent unintended language changes while navigating the text boxes
* [UI] Added more targets


## Bugfixes
* [Bugfix] Fixed "Link" field being ignored by the power generator

### 2.0
## Download

[Download v2.0](https://drive.google.com/open?id=1C_NhPq3lN4_NfEBysfPkrsJ1MqQYvVqO)

(Note: these patch notes were automatically generated from commit messages)

## Features
* [Feature] Added Ritual editor (Beta). It allows you to generate rituals in a veryeasy way, and is also customizable
* [Feature] (Rituals) Added automatic addition of the ritual roll in the macro
* [Feature] (Rituals) Disabling the Ritual roll will hide the character name input box

## Notes

The ritual editor template is customizable. You can find and edit it in **DnD4PowerMacroGenerator_Data > StreamingAssets > Form_Ritual.json**
This is just the beginning: in the next version, I'll add full support for user-made templates. (you can use them already, just copy the Ritual's one for backup and then edit the original one)

### 1.1
## Download

[Download v1.1](https://drive.google.com/open?id=1fDQntArAAfx5n70vcj7S98Bp_vGOQpnG)

(Note: these patch notes were automatically generated from commit messages)

## Features
* [Feature] Added a button to generate a power and increase the index, automatically
* [Feature] It is now possible to save and load power templates

## UI

* [UI] Added program icon 

### 1.0
## Download

[Download v1.0](https://drive.google.com/open?id=1scXc8F-ysME9VCE7rOqOgoixw0D74ApQ)

(Note: these patch notes were automatically generated from commit messages)

## Features
* [Feature] You can specify what kind of damage does the main attack roll
* [Feature] You can now choose  if powers have an attack roll or not
* [Feature] It is now possible to specify if you want to use the "misc" modifier only for damage and attack rolls, or not
* [Feature] It is now possible to use arrow keys to change the UI and Macro language
* [Feature] Added a Preview of how the power would be visualized in roll20's chat
* [Feature] Added an options menu for editing settings
* [Feature] Added dynamic keywords selection
* [Feature] Added some of the standard targets for macro generation
* [Feature] Added the possibility to isnert an Effect and a Miss Effect forthe power
* [Feature] Added UI reset
* [Feature] Added verybasic macro editing and automatic copy to clipboard
* [Feature] Additional hit effects can now be specified
* [Feature] Clicking the macro box will copy the macro to the clipboard
* [Feature] It is now possible to add/remove custom properties when generating a power
* [Feature] The Power Visualizer now shows all the data of the power

## UI Changes

* [UI] Added basic UI for the generator + tab key navigation
* [UI] Added Effect and Miss input boxes, localized all input boxes
* [UI] Added localization support for Targets
* [UI] Added scrollable area for the generator area
* [UI] Added visual feedback for when the macro is copied to the clipboard
* [UI] When you create a Custom property, the cursor automatically focuses on it