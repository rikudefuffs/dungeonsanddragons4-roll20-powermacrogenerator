## Introduction

This generator allows you to easily generate the macro code for power cards that can be used on [roll20](https://roll20.net/), in campaigns that use the [D&D4 Character Sheet template](http://wiki.roll20.net/DnD4e_Character_Sheet). Setting up abilities with the generated code helps in in streamlining the combat process, so you can speed up Roll20 game sessions and not get hung up on dice rolling and looking up abilities.

## Output Examples

![An example of the possible results](https://gitlab.com/rikudefuffs/dungeonsanddragons4-roll20-powermacrogenerator/raw/master/Documentation/OutputExample.jpg)

## Planned Features and Roadmap
See the roadmap [here](https://trello.com/b/vSrlPwyb/dd4-powers-macro-generator-for-roll20)

## Donations
If you find out that this is going to save you some time when building your characters sheet, and want to support development feel free to buy me a beer, donations can be sent [here](https://www.paypal.com/donate/?token=tBPuPNpeTaeDmJaosB-4hcE0_6nizOlkuP53I20_O4GP3pPR6A3fdMp5_wGv3aY2V8LeGm&country.x=IT&locale.x=IT). Also feel free to check my [Youtube](https://www.youtube.com/user/RikuTheFuffs ) channel. Much appreciated! :)

## Downloads

You can check and download the latest version [here](https://gitlab.com/rikudefuffs/dungeonsanddragons4-roll20-powermacrogenerator/-/tags), or directly download the latest version from the button below

[![Download](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Download_Button.svg/1280px-Download_Button.svg.png)](https://drive.google.com/open?id=1C_NhPq3lN4_NfEBysfPkrsJ1MqQYvVqO)