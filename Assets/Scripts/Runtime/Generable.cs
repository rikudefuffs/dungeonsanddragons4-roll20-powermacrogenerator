/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 25/10/2019 15:20:15
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */

using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace DnDGenerator
{
    public abstract class Generable
    {
        protected virtual string[] speciallyManagedProperties { get { return null; } }
        public int index { get; protected set; }
        public PowerUsage useage { get; protected set; }
        public Dictionary<string, string> additionalProperties { get; protected set; }

        /// <summary>
        /// Additional properties that affect the structure of the card power
        /// </summary>
        public Dictionary<string, string> additionalStructuralProperties { get; protected set; }
        public string propertyPrefix { get; protected set; }
        protected StringBuilder stringBuilder;

        public Generable(int index, PowerUsage useage)
        {
            this.index = index;
            this.useage = useage;
            propertyPrefix = string.Format("power-{0}-", this.index);
            additionalProperties = new Dictionary<string, string>();
            additionalStructuralProperties = new Dictionary<string, string>();
            stringBuilder = new StringBuilder();
        }

        public void AddEmote(string emoteText) { additionalStructuralProperties.Add(GeneratorModel.EMOTE_PROPERTY, PowerGenerator.GenerateEmote(emoteText)); }
        public virtual void AddLevel(string className) { additionalStructuralProperties.Add(GeneratorModel.LEVEL_PROPERTY, PowerGenerator.GenerateLevel(propertyPrefix, className)); }
        public void AddLink(string link) { AddProperty(GeneratorModel.LINK_PROPERTY, PowerGenerator.GenerateLink(link)); }
        public void AddProperty(string name, string effect) { additionalProperties.Add(name, effect); }
        public void AddProperties(Dictionary<string, object> properties)
        {
            if (properties == null) { return; }
            foreach (var property in properties)
            {
                if (speciallyManagedProperties.Contains(property.Key))
                {
                    ManageSpecialProperty(property.Key, property.Value);
                    continue;
                }
                AddProperty(property.Key, property.Value.ToString());
            }
        }

        protected abstract void ManageSpecialProperty(string name, object value);
    }
}