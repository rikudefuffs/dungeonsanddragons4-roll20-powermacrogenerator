/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 25/10/2019 15:20:15
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */

using System.Linq;

namespace DnDGenerator
{
    public class Ritual : Generable
    {
        protected override string[] speciallyManagedProperties
        {
            get
            {
                return new string[]
                {
                    GeneratorModel.LINK_PROPERTY, GeneratorModel.LEVEL_PROPERTY,
                    GeneratorModel.EMOTE_PROPERTY, GeneratorModel.RITUAL_ROLL_PROPERTY
                };
            }
        }

        public Ritual(int powerIndex) : base(powerIndex, PowerUsage.Item) { }

        protected override void ManageSpecialProperty(string name, object value)
        {
            switch (name)
            {
                case GeneratorModel.EMOTE_PROPERTY: AddEmote((string)value); break;
                case GeneratorModel.LEVEL_PROPERTY: AddLevel((string)value); break;
                case GeneratorModel.LINK_PROPERTY: AddLink((string)value); break;
                case GeneratorModel.RITUAL_ROLL_PROPERTY: AddRitualRoll((string)value); break;
            }
        }

        public override string ToString()
        {
            stringBuilder.Clear();
            stringBuilder.Append("&{template:dnd4epower}");
            stringBuilder.Append("{{" + useage.ToDescriptionString() + "=1}}");
            foreach (var item in additionalStructuralProperties)
            {
                stringBuilder.Append("{{" + item.Key.ToLower() + "=" + item.Value + "}}");
            }
            stringBuilder.Append("{{name=@{" + propertyPrefix + "name}}}");

            bool addRitualRollAtEnd = additionalProperties.ContainsKey(GeneratorModel.RITUAL_ROLL_PROPERTY);
            foreach (var item in additionalProperties)
            {
                if (addRitualRollAtEnd && (item.Key == GeneratorModel.RITUAL_ROLL_PROPERTY)) { continue; }
                string stringToAdd = Localization.Exists(item.Key) ? (Localization.Get(item.Key) + "**=") : (item.Key + ":**=");
                stringBuilder.Append("{{**" + stringToAdd + item.Value + "}}");
            }

            //removes all variations of newline
            string result = System.Text.RegularExpressions.Regex.Replace(stringBuilder.ToString(), @"\t|\n|\r", "");

            if (!addRitualRollAtEnd) { return result; }

            return result + ("\n " + additionalProperties[GeneratorModel.RITUAL_ROLL_PROPERTY]);
        }

        public void AddRitualRoll(string skillAndCharacter)
        {
            string[] elements = skillAndCharacter.Split('|');
            AddProperty(GeneratorModel.RITUAL_ROLL_PROPERTY, PowerGenerator.GenerateRitualRoll(elements[0], elements[1]));
        }

        public override void AddLevel(string className)
        {
            int temp;
            if (int.TryParse(className, out temp))
            {
                additionalStructuralProperties.Add(GeneratorModel.LEVEL_PROPERTY, PowerGenerator.GenerateLevel(propertyPrefix, string.Empty));
                return;
            }
            base.AddLevel(className);
        }
    }
}