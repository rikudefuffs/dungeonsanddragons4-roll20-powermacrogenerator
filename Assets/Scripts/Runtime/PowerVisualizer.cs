/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 27/10/2019 19:05:14
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using TMPro;
using System.Text.RegularExpressions;
namespace DnDGenerator
{
    ///<summary>
    ///provides a real-time visualization of how the power would appear on Roll20
    ///</summary>
    public class PowerVisualizer : MonoBehaviour
    {
        static readonly Color32 lightBackgroundColor = new Color32(243, 240, 237, 255);
        static readonly Color32 darkBackgroundColor = new Color32(235, 230, 225, 255);

        static readonly Color32 atWillPowerColor = new Color32(58, 112, 66, 255);
        static readonly Color32 encounterPowerColor = new Color32(125, 8, 35, 255);
        static readonly Color32 dailyPowerColor = new Color32(51, 51, 51, 255);
        static readonly Color32 ItemPowerColor = new Color32(230, 184, 0, 255);
        static readonly Color32 abilityPowerColor = new Color32(72, 61, 139, 255);
        static readonly Color32 skillPowerColor = new Color32(58, 94, 202, 255);
        static readonly Color32 otherPowerColor = new Color32(0, 0, 0, 255);
        Generable power;
        Dictionary<string, string> dummyData;

        public Image imgTitleBackground;
        public TMP_Text lblEmote;
        public TMP_Text lblPowerName;
        public TMP_Text lblPowerClassAndLevel;
        public TMP_Text lblUsageAndKeywords;
        public TMP_Text lblActionTypeAndRange;

        public VerticalLayoutGroup elementsContainer;
        public VisualizerRow rowPrefab;
        Color32 nextColorToUse
        {
            get
            {
                lastColorUsed = lastColorUsed.Compare(lightBackgroundColor) ? darkBackgroundColor : lightBackgroundColor;
                return lastColorUsed;
            }
        }
        Color32 lastColorUsed;

        void Awake()
        {
            dummyData = new Dictionary<string, string>();
            dummyData.Add("character_name", "RikuTheFuffs");
            dummyData.Add("name", "Super Power Name");
            dummyData.Add("useage", "Daily"); //can be gathered from data
            dummyData.Add("range", "10");
            dummyData.Add("action", "Standard");
            dummyData.Add("PowerLevel", Random.Range(1, 31).ToString());
        }

        public void Visualize(Ritual ritual)
        {
            this.power = ritual;
            UIUtilities.ClearContainer(elementsContainer, lblUsageAndKeywords.transform.parent);
            BuildEmote();
            BuildTitle();
            BuildAdditionalProperties();
        }

        public void Visualize(Power power)
        {
            this.power = power;
            UIUtilities.ClearContainer(elementsContainer, lblUsageAndKeywords.transform.parent, lblActionTypeAndRange.transform.parent);
            BuildEmote();
            BuildTitle();
            BuildUsageAndKeywords();
            BuildActionTypeAndRange();
            BuildTarget();
            BuildAdditionalProperties();
        }

        void BuildEmote()
        {
            if (power.additionalStructuralProperties.ContainsKey(GeneratorModel.EMOTE_PROPERTY))
            {
                lblEmote.gameObject.SetActive(true);
                lblEmote.text = power.additionalStructuralProperties[GeneratorModel.EMOTE_PROPERTY].Replace("@{character_name}", dummyData["character_name"]);
                return;
            }
            lblEmote.gameObject.SetActive(false);
        }

        void BuildTitle()
        {
            imgTitleBackground.color = GetColorForPowerUsage(power.useage);
            lblPowerName.text = dummyData["name"];
            if (power.additionalStructuralProperties.ContainsKey(GeneratorModel.LEVEL_PROPERTY))
            {
                lblPowerClassAndLevel.gameObject.SetActive(true);
                lblPowerClassAndLevel.text = power.additionalStructuralProperties[GeneratorModel.LEVEL_PROPERTY].Split('@')[0] + dummyData["PowerLevel"];
            }
            else
            {
                lblPowerClassAndLevel.gameObject.SetActive(false);
            }
        }

        void BuildUsageAndKeywords()
        {
            lblUsageAndKeywords.transform.parent.GetComponent<Image>().color = darkBackgroundColor;
            lblUsageAndKeywords.text = Localization.Get(power.useage.ToDescriptionString()) + " ♦ " + string.Join(",", (power as Power).keywords);
        }

        void BuildActionTypeAndRange()
        {
            lblActionTypeAndRange.transform.parent.GetComponent<Image>().color = lightBackgroundColor;
            lblActionTypeAndRange.text = dummyData["action"] + " ♦ " + dummyData["range"];
            lastColorUsed = lightBackgroundColor;
        }

        void BuildTarget()
        {
            VisualizerRow row = Instantiate(rowPrefab, elementsContainer.transform);
            row.Initialize(nextColorToUse, string.Format("<b>Target:</b> {0}", Localization.Get((power as Power).target)));
        }

        void BuildAdditionalProperties()
        {
            foreach (var item in power.additionalProperties)
            {
                string stringToAdd = Localization.Exists(item.Key) ? Localization.Get(item.Key) : (item.Key + ":");
                VisualizerRow row = Instantiate(rowPrefab, elementsContainer.transform);
                if (item.Key == GeneratorModel.LINK_PROPERTY)
                {
                    string link = Regex.Match(item.Value, @"\(.*?\)").Value.Trim('(',')');
                    row.Initialize(nextColorToUse, string.Format("<b>{0}</b> <link=\"{1}\">{2}</link>", stringToAdd, link, Localization.Get("LinkInMacro").Trim('[', ']')));
                    continue;
                }
                row.Initialize(nextColorToUse, string.Format("<b>{0}</b> {1}", stringToAdd, ConvertRoll20Expression(item.Value)));
            }
        }

        /// <summary>
        /// Converts a roll20 expression (I.E: [[1d20 + 100]]) to a random integer value
        /// </summary>
        /// <param name="expression"></param>
        /// <returns>A string that represents a random, fake result of the roll20 expression</returns>
        public static string ConvertRoll20Expression(string expression)
        {
            expression = expression.Replace("**", string.Empty);
            expression = Regex.Replace(expression, @"\[\[.*?\]\]", Random.Range(1, 100).ToString());
            //specific for defenses, which have a special syntax
            expression = Regex.Replace(expression, @"@{.*?-def}", GeneratorModel.attackRollsDefenses[Random.Range(1, 5)]);
            return Regex.Replace(expression, @"@{.*?}", Random.Range(1, 100).ToString());
        }

        public static Color GetColorForPowerUsage(PowerUsage useage)
        {
            switch (useage)
            {
                case PowerUsage.AtWill: return atWillPowerColor;
                case PowerUsage.Encounter: return encounterPowerColor;
                case PowerUsage.Daily: return dailyPowerColor;
                case PowerUsage.Item: return ItemPowerColor;
                case PowerUsage.Ability: return abilityPowerColor;
                case PowerUsage.Skill: return skillPowerColor;
                case PowerUsage.Other: return otherPowerColor;
            }
            return Color.black;
        }
    }
}