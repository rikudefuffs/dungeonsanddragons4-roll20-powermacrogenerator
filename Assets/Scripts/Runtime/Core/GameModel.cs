/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Original Author: Abela Paolo
 * Creation Date: 10/23/2016 8:10:25 PM
 * Updates: 
 * 
 * Copyright � 2016 StarworkGC
 * ----------------------------------------------
 */

using SmartMVC;

namespace DnDGenerator
{
    ///<summary>
    /// Game Application Main model
    ///</summary>
    public class GameModel : Model<GameApplication>
    {
        public GeneratorModel generator { get { return generatorModel = Find(generatorModel); } }
        GeneratorModel generatorModel;

        public DynamicFormModel form { get { return formModel = Find(formModel); } }
        DynamicFormModel formModel;
    }
}