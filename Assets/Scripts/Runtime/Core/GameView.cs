/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Original Author: Abela Paolo
 * Creation Date: 10/23/2016 8:09:59 PM
 * Updates: 
 * 
 * Copyright � 2016 StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using SmartMVC;

namespace DnDGenerator
{
    ///<summary>
    ///Game Application Main view
    ///</summary>
    public class GameView : View<GameApplication>
    {
        public GeneratorView generator { get { return generatorView = Find(generatorView); } }
        GeneratorView generatorView;

        public SettingsView settings { get { return settingsView = Find(settingsView); } }
        SettingsView settingsView;

        public DynamicFormView form { get { return formView = Find(formView); } }
        DynamicFormView formView;
    }
}