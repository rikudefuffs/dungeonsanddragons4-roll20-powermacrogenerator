/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Original Author: Abela Paolo
 * Creation Date: 10/23/2016 8:09:27 PM
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using SmartMVC;

namespace DnDGenerator
{
    ///<summary>
    /// The Game application core
    ///</summary>

    public class GameApplication : BaseApplication<GameModel, GameView, GameController>
    {
        public static GameApplication instance;
        protected override void Awake()
        {
            base.Awake();
            instance = this;
            Application.targetFrameRate = 30;
        }

        /// <summary>
        /// Broadcast the specified message to the entire UI.
        /// </summary>
        public static void Broadcast(string funcName)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
#endif
            {
                
                instance.BroadcastMessage(funcName, SendMessageOptions.DontRequireReceiver);
            }
        }

        /// <summary>
        /// Broadcast the specified message to the entire UI.
        /// </summary>

        public static void Broadcast(string funcName, object param)
        {
            if (param == null)
            {
                // More on this: http://answers.unity3d.com/questions/55194/suggested-workaround-for-sendmessage-bug.html
                Debug.LogError("SendMessage is bugged when you try to pass 'null' in the parameter field. It behaves as if no parameter was specified.");
            }
            else
            {
                instance.BroadcastMessage(funcName, param, SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}