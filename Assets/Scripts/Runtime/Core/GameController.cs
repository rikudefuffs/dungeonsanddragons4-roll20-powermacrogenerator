/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Original Author: Abela Paolo
 * Creation Date: 10/23/2016 8:10:35 PM
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using SmartMVC;

namespace DnDGenerator
{
    ///<summary>
    ///Game Application Main controller
    ///</summary>
    public class GameController : Controller<GameApplication>
    {
    }
}