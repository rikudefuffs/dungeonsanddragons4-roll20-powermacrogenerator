/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 25/10/2019 15:20:15
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */

using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace DnDGenerator
{
    ///<summary>
    ///Holds all information about a spcific Power
    ///</summary>
    public class Power : Generable
    {
        protected override string[] speciallyManagedProperties
        {
            get
            {
                return new string[]
                {
                    GeneratorModel.ATTACK_ROLL_PROPERTY, GeneratorModel.HIT_PROPERTY,
                    GeneratorModel.LINK_PROPERTY, GeneratorModel.LEVEL_PROPERTY, GeneratorModel.EMOTE_PROPERTY
                };
            }
        }

        public string[] keywords { get; private set; }
        public string target { get; private set; }

        public Power(int powerIndex, PowerUsage powerUsage, string[] keywords, string target) : base(powerIndex, powerUsage)
        {
            this.keywords = keywords;
            this.target = target;
        }

        public void AddAttack(AttackRollData data) { AddProperty(GeneratorModel.ATTACK_ROLL_PROPERTY, PowerGenerator.GenerateAttack(propertyPrefix, data.useOnlyMiscModifier)); }
        public void AddHit(DamageRollData data) { AddProperty(GeneratorModel.HIT_PROPERTY, PowerGenerator.GenerateHit(propertyPrefix, data)); }

        protected override void ManageSpecialProperty(string name, object value)
        {
            switch (name)
            {
                case GeneratorModel.ATTACK_ROLL_PROPERTY: AddAttack((AttackRollData)value); break;
                case GeneratorModel.HIT_PROPERTY: AddHit((DamageRollData)value); break;
                case GeneratorModel.EMOTE_PROPERTY: AddEmote((string)value); break;
                case GeneratorModel.LEVEL_PROPERTY: AddLevel((string)value); break;
                case GeneratorModel.LINK_PROPERTY: AddLink((string)value); break;
                case GeneratorModel.MISS_PROPERTY: AddProperty(name, ((string)value)); break;
                case GeneratorModel.EFFECT_PROPERTY: AddProperty(name, ((string)value)); break;
            }
        }

        public override string ToString()
        {
            stringBuilder.Clear();
            stringBuilder.Append("&{template:dnd4epower}");
            stringBuilder.Append("{{" + useage.ToDescriptionString() + "=1}}");
            foreach (var item in additionalStructuralProperties)
            {
                stringBuilder.Append("{{" + item.Key.ToLower() + "=" + item.Value + "}}");
            }
            stringBuilder.Append("{{name=@{" + propertyPrefix + "name}}}");
            stringBuilder.Append("{{type=@{" + propertyPrefix + "useage} ♦}}");
            stringBuilder.Append("{{keywords=" + string.Join(", ", keywords) + "}}");
            stringBuilder.Append("{{action=@{" + propertyPrefix + "action} ♦}}");
            stringBuilder.Append("{{range=@{" + propertyPrefix + "range}}}");
            stringBuilder.Append("{{target=" + Localization.Get(target) + "}}");
            foreach (var item in additionalProperties)
            {
                string stringToAdd = Localization.Exists(item.Key) ? (Localization.Get(item.Key) + "**=") : (item.Key + ":**=");
                stringBuilder.Append("{{**" + stringToAdd + item.Value + "}}");
            }
            //removes all variations of newline
            return System.Text.RegularExpressions.Regex.Replace(stringBuilder.ToString(), @"\t|\n|\r", "");
        }
    }
}