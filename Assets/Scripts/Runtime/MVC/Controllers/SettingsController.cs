/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 28/10/2019 11:34:05
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using SmartMVC;

namespace DnDGenerator
{
    ///<summary>
    ///Handles the logic of the SettingsView and helps it communicating with the model
    ///</summary>
    public class SettingsController : Controller<GameApplication>
    {
        SettingsView settingsView { get { return app.view.settings; } }

        void OnEnable()
        {
            AddEventListenerToApp(MVCEvents.OPEN_OPTIONS, OpenOptionsView);
        }

        void OnDisable()
        {
            RemoveEventListenerFromApp(MVCEvents.OPEN_OPTIONS, OpenOptionsView);
        }

        void OpenOptionsView(params object[] data)
        {
            settingsView.gameObject.SetActive(true);
        }
    }
}