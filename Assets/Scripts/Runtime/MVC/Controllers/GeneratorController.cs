﻿/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 25/10/2019 15:20:15
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;

using System.Collections.Generic;
using System.Linq;
using SimpleJSON;
using SmartMVC;

namespace DnDGenerator
{
    ///<summary>
    ///Handles the logic of the GeneratorView and helps it communicating with the model
    ///</summary>
    public class GeneratorController : Controller<GameApplication>
    {
        GeneratorModel generatorModel { get { return app.model.generator; } }
        GeneratorView generatorView { get { return app.view.generator; } }

        void OnEnable()
        {
            AddEventListenerToApp(MVCEvents.GENERATE_MACRO, GenerateMacro);
            AddEventListenerToApp(MVCEvents.GENERATOR_RESET, ResetGenerator);
            AddEventListenerToApp(MVCEvents.GENERATOR_LOAD, LoadTemplate);
            AddEventListenerToApp(MVCEvents.GENERATOR_SAVE, SaveTemplate);
            AddEventListenerToApp(MVCEvents.FORM_INDEX_CHANGED, OnIndexChanged);
            AddEventListenerToApp(MVCEvents.FORM_EMOTE_CHANGED, OnEmoteChanged);
            AddEventListenerToApp(MVCEvents.FORM_LEVEL_CHANGED, OnLevelChanged);
            AddEventListenerToApp(MVCEvents.FORM_LINK_CHANGED, OnLinkChanged);
            AddEventListenerToApp(MVCEvents.FORM_USEAGE_CHANGED, OnUsageTypeChanged);
            AddEventListenerToApp(MVCEvents.FORM_FORCE_UPDATE, OnForceFormValuesUpdate);
        }

        void OnDisable()
        {
            RemoveEventListenerFromApp(MVCEvents.FORM_FORCE_UPDATE, OnForceFormValuesUpdate);
            RemoveEventListenerFromApp(MVCEvents.FORM_USEAGE_CHANGED, OnUsageTypeChanged);
            RemoveEventListenerFromApp(MVCEvents.FORM_LINK_CHANGED, OnLinkChanged);
            RemoveEventListenerFromApp(MVCEvents.FORM_LEVEL_CHANGED, OnLevelChanged);
            RemoveEventListenerFromApp(MVCEvents.FORM_EMOTE_CHANGED, OnEmoteChanged);
            RemoveEventListenerFromApp(MVCEvents.FORM_INDEX_CHANGED, OnIndexChanged);
            RemoveEventListenerFromApp(MVCEvents.GENERATOR_SAVE, SaveTemplate);
            RemoveEventListenerFromApp(MVCEvents.GENERATOR_LOAD, LoadTemplate);
            RemoveEventListenerFromApp(MVCEvents.GENERATOR_RESET, ResetGenerator);
            RemoveEventListenerFromApp(MVCEvents.GENERATE_MACRO, GenerateMacro);
        }

        void SaveTemplate(params object[] data)
        {
            string filePath = (string)(data[0]);
            JSONNode powerData = JSONNode.Parse(GeneratePowerData());
            JSONUtilities.WriteJSONToFile(filePath, powerData);
            Debug.Log("File saved successfully");
            //GameApplication.instance.ShowMessage(Localization.Get("SavedSuccessfully"), false, false, true);
        }

        void LoadTemplate(params object[] data)
        {
            string filePath = (string)(data[0]);
            JSONNode powerData = JSONUtilities.ReadJSONFromFile(filePath);
            if (powerData == null)
            {
                Debug.LogError("Invalid File");
                //GameApplication.instance.ShowMessage(Localization.Get("InvalidFile"), true, false, true);
                return;
            }
            FillUIWithPowerData(powerData);
        }

        void ResetGenerator(params object[] data)
        {
            generatorModel.powerKeywords.Clear();
            generatorModel.ClearProperties();
            generatorView.ResetUI();
        }

        void GenerateMacro(params object[] data)
        {
            bool isGeneratingPower = (bool)data[0];
            if (isGeneratingPower)
            {
                GeneratePowerMacro();
                return;
            }
            GenerateRitualMacro();
        }
        void GeneratePowerMacro()
        {
            Dictionary<string, object> additionalProperties = new Dictionary<string, object>();
            if (generatorModel.hasAttackRoll)
            {
                bool useOnlyMiscModifier = generatorModel.useOnlyMiscValueForDamageAndAttackRolls;
                AttackRollData attackRollData = new AttackRollData(generatorModel.defenseFromAttackRoll, useOnlyMiscModifier);
                additionalProperties.Add(GeneratorModel.ATTACK_ROLL_PROPERTY, attackRollData);

                DamageType damageType = generatorModel.damageRollType;
                int flatDamage = 0;
                DamageRollData damageRollData = new DamageRollData(useOnlyMiscModifier, damageType, flatDamage, generatorModel.additionalHitEffect);
                additionalProperties.Add(GeneratorModel.HIT_PROPERTY, damageRollData);
            }

            if (!generatorModel.effect.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.EFFECT_PROPERTY, generatorModel.effect);
            }

            if (!generatorModel.missEffect.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.MISS_PROPERTY, generatorModel.missEffect);
            }
            if (!generatorModel.emote.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.EMOTE_PROPERTY, generatorModel.emote);
            }
            if (!generatorModel.level.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.LEVEL_PROPERTY, generatorModel.level);
            }

            foreach (var property in generatorModel.customProperties)
            {
                additionalProperties.Add(property.Key, property.Value.GetValue());
            }

            if (!generatorModel.link.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.LINK_PROPERTY, generatorModel.link);
            }

            Power power = PowerGenerator.GenerateAndReturn
            (
                generatorModel.powerIndex, generatorModel.powerUsage,
                generatorModel.powerKeywords.Select(s => Localization.Get(s)).ToArray(),
                generatorModel.powerTarget, additionalProperties
            );

            string macro = power.ToString();
            generatorView.inputMacro.SetTextWithoutNotify(macro);
            macro.CopyToClipboard();
            generatorView.OnMacroCopied();
            generatorView.powerVisualizer.Visualize(power);
        }


        void GenerateRitualMacro(params object[] data)
        {
            Dictionary<string, object> additionalProperties = new Dictionary<string, object>();
            List<string> customPropertiesToIgnore = new List<string>();
            customPropertiesToIgnore.Add(GeneratorModel.RITUAL_ROLL_PROPERTY);
            customPropertiesToIgnore.Add("characterNameForRoll");
            bool addRitualRoll = (bool)generatorModel.customProperties[GeneratorModel.RITUAL_ROLL_PROPERTY].GetValue();
            if (addRitualRoll)
            {
                additionalProperties.Add(GeneratorModel.RITUAL_ROLL_PROPERTY, string.Format("{0}|{1}", generatorModel.customProperties["ability"].GetValue().ToString(), generatorModel.customProperties["characterNameForRoll"].GetValue()));
            }

            if (!generatorModel.effect.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.EFFECT_PROPERTY, generatorModel.effect);
            }

            if (!generatorModel.emote.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.EMOTE_PROPERTY, generatorModel.emote);
            }
            if (!generatorModel.level.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.LEVEL_PROPERTY, generatorModel.level);
            }
            else
            {
                additionalProperties.Add(GeneratorModel.LEVEL_PROPERTY, string.Empty);
            }

            foreach (var property in generatorModel.customProperties)
            {
                if (customPropertiesToIgnore.Contains(property.Key)) { continue; }
                additionalProperties.Add(property.Key, property.Value.GetValue());
            }

            if (!generatorModel.link.IsNullOrEmpty())
            {
                additionalProperties.Add(GeneratorModel.LINK_PROPERTY, generatorModel.link);
            }

            Ritual ritual = PowerGenerator.GenerateAndReturn
            (
                generatorModel.powerIndex, additionalProperties
            );
            string macro = ritual.ToString();
            generatorView.inputMacro.SetTextWithoutNotify(macro);
            macro.CopyToClipboard();
            generatorView.OnMacroCopied();
            generatorView.powerVisualizer.Visualize(ritual);
        }

        void Start()
        {
            generatorView.lblLanguage.text = Localization.language;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (Input.GetKey(KeyCode.LeftControl)
                || Input.GetKey(KeyCode.LeftCommand)
                || Input.GetKey(KeyCode.RightControl)
                || Input.GetKey(KeyCode.RightCommand))
                {
                    ChangeLocalization(1);
                }
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (Input.GetKey(KeyCode.LeftControl)
                || Input.GetKey(KeyCode.LeftCommand)
                || Input.GetKey(KeyCode.RightControl)
                || Input.GetKey(KeyCode.RightCommand))
                {
                    ChangeLocalization(-1);
                }
            }
        }

        void ChangeLocalization(int num)
        {
            for (int i = 0; i < Localization.knownLanguages.Length; i++)
            {
                if (Localization.knownLanguages[i] != Localization.language) { continue; }
                Localization.language = Localization.knownLanguages[StarworkMath.Modulo(i + num, Localization.knownLanguages.Length)];
                break;
            }
            generatorView.lblLanguage.text = Localization.language;
        }

        string GeneratePowerData()
        {
            JSONNode powerData = JSONNode.Parse("{}");
            powerData["index"].AsInt = generatorModel.powerIndex;
            powerData["useage"].AsInt = (int)generatorModel.powerUsage;
            JSONArray keywords = new JSONArray();
            foreach (var item in generatorModel.powerKeywords)
            {
                keywords.Add(item);
            }
            powerData["keywords"] = keywords;
            powerData["target"].AsInt = generatorModel.powerTargetIndex;
            powerData["defenseFromAttackRoll"].AsInt = generatorModel.defenseFromAttackRollIndex;
            powerData["damageRollType"].AsInt = (int)generatorModel.damageRollType;
            powerData["additionalHitEffect"] = generatorModel.additionalHitEffect;
            powerData["effect"] = generatorModel.effect;
            powerData["missEffect"] = generatorModel.missEffect;
            powerData["emote"] = generatorModel.emote;
            powerData["level"] = generatorModel.level;
            powerData["link"] = generatorModel.link;
            powerData["useOnlyMiscValueForDamageAndAttackRolls"].AsBool = generatorModel.useOnlyMiscValueForDamageAndAttackRolls;
            JSONArray customProperties = new JSONArray();
            foreach (var property in generatorModel.customProperties)
            {
                JSONNode dataRow = JSONNode.Parse("{}");
                dataRow.Add(property.Key, property.Value.GetValue().ToString());
                customProperties.Add(dataRow);
            }
            powerData["customProperties"] = customProperties;
            return powerData.ToString();
        }

        void FillUIWithPowerData(JSONNode powerData)
        {
            generatorView.inputIndex.text = powerData["index"].Value;
            generatorView.lstUsage.value = powerData["useage"].AsInt;

            generatorModel.powerKeywords = powerData["keywords"].AsArray.Childs.Select(j => j.Value).ToList();
            generatorView.ForceKeywordsToggleUIOn(generatorModel.powerKeywords);

            generatorView.lstTarget.value = powerData["target"].AsInt;
            generatorView.lstAttackRollDefenses.value = powerData["defenseFromAttackRoll"].AsInt;
            generatorView.lstDamageTypes.value = powerData["damageRollType"].AsInt;
            generatorView.inputAdditionalHitEffect.text = powerData["additionalHitEffect"].Value;
            generatorView.inputEffect.text = powerData["effect"].Value;
            generatorView.inputMissEffect.text = powerData["missEffect"].Value;
            generatorView.inputEmote.text = powerData["emote"].Value;
            generatorView.inputLevel.text = powerData["level"].Value;
            generatorView.inputLink.text = powerData["link"].Value;
            generatorView.tglUseMiscOnly.isOn = powerData["useOnlyMiscValueForDamageAndAttackRolls"].AsBool;

            foreach (var property in powerData["customProperties"].AsArray.Childs)
            {
                string propertyName = property.Keys.First();
                generatorView.AddCustomProperty(propertyName, property[propertyName]);
            }
            generatorView.ForceValuesUpdate();
        }

        void OnForceFormValuesUpdate(params object[] data)
        {
            foreach (var item in generatorModel.standardProperties)
            {
                item.Value.ForceUpdate();
            }
        }

        void OnUsageTypeChanged(params object[] data)
        {
            string newUsage = (string)data[0];
            if (newUsage.IsNullOrEmpty()) { return; }
            generatorModel.powerUsage = (PowerUsage)System.Convert.ToInt32(newUsage);
        }

        void OnIndexChanged(params object[] data)
        {
            string newIndex = (string)data[0];
            if (newIndex.IsNullOrEmpty()) { return; }
            generatorModel.powerIndex = System.Convert.ToInt32(newIndex);
        }
        void OnEmoteChanged(params object[] data) { generatorModel.emote = (string)data[0]; }
        void OnLevelChanged(params object[] data) { generatorModel.level = (string)data[0]; }
        void OnLinkChanged(params object[] data) { generatorModel.link = (string)data[0]; }
    }
}