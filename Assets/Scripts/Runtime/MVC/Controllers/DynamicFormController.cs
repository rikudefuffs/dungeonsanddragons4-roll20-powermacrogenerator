/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 22/03/2020 21:42:16
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using SmartMVC;
using SimpleJSON;
using System.Collections.Generic;

namespace DnDGenerator
{
    ///<summary>
    /// The controller of a dynamically generated form
    ///</summary>
    public class DynamicFormController : Controller<GameApplication>
    {
        DynamicFormView formView { get { return app.view.form; } }
        DynamicFormModel formModel { get { return app.model.form; } }
        GeneratorModel generatorModel { get { return app.model.generator; } }


        void OnEnable()
        {
            AddEventListenerToApp(MVCEvents.FORM_LOAD, LoadForm);
            AddEventListenerToApp(MVCEvents.FORM_ADD_RITUAL_ROLL_CHANGED, OnAddRitualRollChanged);
        }

        void OnDisable()
        {
            RemoveEventListenerFromApp(MVCEvents.FORM_LOAD, LoadForm);
            RemoveEventListenerFromApp(MVCEvents.FORM_ADD_RITUAL_ROLL_CHANGED, OnAddRitualRollChanged);
        }

        void LoadForm(params object[] data)
        {
            ClearForm();
            JSONNode form = (JSONNode)data[0];

            LoadStandardProperties(form);
            LoadCustomProperties(form);
            AddCustomFieldInputBox();

            Notify(MVCEvents.FORM_FORCE_UPDATE);
        }

        void LoadStandardProperties(JSONNode form)
        {
            foreach (var formElement in form.ChildsWithKeys)
            {
                if (formElement.Key == "customProperties") { continue; }
                switch (formElement.Key)
                {
                    case "Index": formView.AddStandardProperty(formElement.Key, formElement.Value, (data) => Notify(MVCEvents.FORM_INDEX_CHANGED, data)); break;
                    case "useage": formView.AddStandardProperty(formElement.Key, formElement.Value, (data) => Notify(MVCEvents.FORM_USEAGE_CHANGED, data)); break;
                    case "Emote": formView.AddStandardProperty(formElement.Key, formElement.Value, (data) => Notify(MVCEvents.FORM_EMOTE_CHANGED, data)); break;
                    case "Level": formView.AddStandardProperty(formElement.Key, formElement.Value, (data) => Notify(MVCEvents.FORM_LEVEL_CHANGED, data)); break;
                    case "Link": formView.AddStandardProperty(formElement.Key, formElement.Value, (data) => Notify(MVCEvents.FORM_LINK_CHANGED, data)); break;
                    default: break;
                }
            }
        }

        void LoadCustomProperties(JSONNode form)
        {
            foreach (JSONNode customPropertyElement in form["customProperties"].AsArray) //Debug.LogError("Adding array: " + item.Key);
            {
                foreach (var formElement in customPropertyElement.ChildsWithKeys) //always one, unless field is empty (returns null in that case)
                {
                    switch (formElement.Key)
                    {
                        case GeneratorModel.RITUAL_ROLL_PROPERTY: formView.AddCustomProperty(formElement.Key, formElement.Value, (data) => Notify(MVCEvents.FORM_ADD_RITUAL_ROLL_CHANGED, data)); break;
                        case "characterNameForRoll": formView.AddCustomProperty(formElement.Key, formElement.Value, (data) => Notify(MVCEvents.FORM_CHARACTER_OF_RITUAL_ROLL_CHANGED, data)); break;
                        default: formView.AddCustomProperty(formElement.Key, formElement.Value); break;//Debug.LogError("Adding child of array: " + customElement.Value);
                    }
                }
            }
        }

        void AddCustomFieldInputBox()
        {
            formView.AddCustomPropertyInputBox();
        }

        void ClearForm()
        {
            app.view.form.DestroyPreviousGridControls(); //[TODO] remove when adding switching between forms

            Dictionary<string, string> formValues = app.model.form.formValues;
            foreach (var formElement in formValues)
            {
                app.view.form.RemoveElement(formElement.Key);
            }
            formValues.Clear();
        }

        void OnAddRitualRollChanged(params object[] data)
        {
            if ((generatorModel.customProperties == null) || !generatorModel.customProperties.ContainsKey("characterNameForRoll")) { return; }
            generatorModel.customProperties["characterNameForRoll"].SetVisibility((bool)data[0]);
        }
    }
}