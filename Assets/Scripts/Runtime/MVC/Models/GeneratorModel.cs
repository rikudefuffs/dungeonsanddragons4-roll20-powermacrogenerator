/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 25/10/2019 15:40:40
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmartMVC;
using UnityEngine.UI;

namespace DnDGenerator
{
    ///<summary>
    /// Holds the data of the Generator's UI
    ///</summary>
    public class GeneratorModel : Model<GameApplication>
    {
        public const string ATTACK_ROLL_PROPERTY = "Attack";
        public const string HIT_PROPERTY = "Hit";
        public const string MISS_PROPERTY = "Miss";
        public const string EFFECT_PROPERTY = "Effect";
        public const string EMOTE_PROPERTY = "Emote";
        public const string LEVEL_PROPERTY = "Level";
        public const string LINK_PROPERTY = "Link";
        public const string RITUAL_ROLL_PROPERTY = "addRitualRoll";
        public const string CHARACTER_NAME_FOR_ROLL_PROPERTY = "characterNameForRoll";

        public static readonly string[] keywords = new string[]  //full list here: https://www.dandwiki.com/wiki/Power_Design_(4e_Guideline)
        {
            "Weapon","Implement", //Power tool
            "Arcane","Divine","Martial","Primal","Psionic","Shadow", //Power Source
            "Acid","Cold","Fire","Force","Lightning","Necrotic","Poison","Psychic","Radiant","Thunder", //damage types
            //Effect keywords
            "Augment","Aura","Beast","Beast Form","Channel Divinity","Charm","Conjuration","Disease","Fear","Full Discipline",
            "Healing","Illusion","Invigorating","Poison","Polymorph","Reliable","Rage","Rattling","Runic","Sleep","Spirit",
            "Stance","Summoning","Teleportation","Zone"
        };

        public static readonly string[] standardTargets = new string[]  //full list here: https://www.dandwiki.com/wiki/Power_Design_(4e_Guideline)
        {
            "TARGET_OneCreature",
            "TARGET_Personal",
            "TARGET_AnAlly",
            "TARGET_YourOrAnAlly",
            "TARGET_AllCreaturesInArea",
            "TARGET_AllEnemiesInArea",
            "TARGET_YouOrAllyInArea",
            "TARGET_HitCreature",
            "TARGET_EveryEnemyYouCanSee",
            "TARGET_TriggeringEnemy",
            "TARGET_TriggeringAlly",
            "TARGET_YouOrAllyBloodied",
            "TARGET_Weapon",
            "TARGET_WeaponOrImplement"
        };

        public static readonly string[] attackRollsDefenses = new string[]
        {
            "None","AC","Fort","Ref","Will"
        };

        public int powerIndex;
        public PowerUsage powerUsage;
        public int defenseFromAttackRollIndex;
        public int powerTargetIndex;
        public List<string> powerKeywords;
        public DamageType damageRollType;
        public string additionalHitEffect;
        public string effect;
        public string missEffect;
        public string emote;
        public string level;
        public string link;
        public bool useOnlyMiscValueForDamageAndAttackRolls;
        public Dictionary<string, FormElement> standardProperties;
        public Dictionary<string, FormElement> customProperties;

        public string defenseFromAttackRoll { get { return attackRollsDefenses[defenseFromAttackRollIndex]; } }
        public string powerTarget { get { return standardTargets[powerTargetIndex]; } }
        public bool hasAttackRoll { get { return defenseFromAttackRoll != attackRollsDefenses[0]; } }
        public bool isGeneratingPower;

        void Awake()
        {
            powerKeywords = new List<string>();
            standardProperties = new Dictionary<string, FormElement>();
            customProperties = new Dictionary<string, FormElement>();
            defenseFromAttackRollIndex = 0;
            powerTargetIndex = 0;
            isGeneratingPower = true;
        }

        public void RemoveCustomProperty(string name)
        {
            customProperties[name].Remove();
            customProperties.Remove(name);
        }

        public void ClearProperties()
        {
            foreach (var field in standardProperties)
            {
                field.Value.Remove();
            }
            standardProperties.Clear();

            foreach (var field in customProperties)
            {
                field.Value.Remove();
            }
            customProperties.Clear();
        }
    }

    /// <summary>
    /// A generic element that can be added to a form
    /// </summary>
    public abstract class FormElement
    {
        public delegate void NotifyAction(params object[] data);
        
        public TMPro.TMP_Text label;
        protected NotifyAction valueChangedCallback;

        public FormElement(TMPro.TMP_Text label, NotifyAction OnValueChanged)
        {
            this.label = label;
            valueChangedCallback = OnValueChanged;
        }

        protected void Notify(string newValue) { valueChangedCallback.Invoke(newValue); }

        public virtual void Remove()
        {
            GameObject.Destroy(label.gameObject);
        }
        public virtual void SetVisibility(bool isVisible)
        {
            label.gameObject.SetActive(isVisible);
        }

        public abstract object GetValue();
        public abstract void ForceUpdate();
    }

    /// <summary>
    /// An element represented by a label and a text field
    /// </summary>
    public class TextFormElement : FormElement
    {
        public TMPro.TMP_InputField inputField;

        public TextFormElement(TMPro.TMP_Text label, TMPro.TMP_InputField inputField, NotifyAction OnValueChanged) : base(label, OnValueChanged)
        {
            this.inputField = inputField;
            
            if (valueChangedCallback == null) { return; }

            this.inputField.onValueChanged.AddListener(Notify);
        }

        public override void Remove()
        {
            base.Remove();
            GameObject.Destroy(inputField.gameObject);
        }

        public override void SetVisibility(bool isVisible)
        {
            base.SetVisibility(isVisible);
            inputField.gameObject.SetActive(isVisible);
        }

        public override void ForceUpdate()
        {
            if (valueChangedCallback == null) { return; }
            Notify(inputField.text);
        }

        public override object GetValue() { return inputField.text; }
    }

    /// <summary>
    /// An element represented by a label and a text field
    /// </summary>
    public class ToggleFormElement : FormElement
    {
        public Toggle toggle;

        public ToggleFormElement(TMPro.TMP_Text label, Toggle toggle, NotifyAction OnValueChanged) : base(label, OnValueChanged)
        {
            this.toggle = toggle;
            
            if (valueChangedCallback == null) { return; }

            this.toggle.onValueChanged.AddListener(Notify);
        }

        public override void Remove()
        {
            base.Remove();
            GameObject.Destroy(toggle.gameObject);
        }

        public override void SetVisibility(bool isVisible)
        {
            base.SetVisibility(isVisible);
            toggle.gameObject.SetActive(isVisible);
        }

        public override void ForceUpdate()
        {
            if (valueChangedCallback == null) { return; }
            Notify(toggle.isOn);
        }
        protected void Notify(bool newValue) { valueChangedCallback.Invoke(newValue); }

        public override object GetValue() { return toggle.isOn; }
    }
}