/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 22/03/2020 22:02:31
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using SmartMVC;
using System.Collections.Generic;
using UnityEngine.UI;

namespace DnDGenerator
{
    ///<summary>
    ///
    ///</summary>
    public class DynamicFormModel : Model<GameApplication>
    {
        public Dictionary<string, string> formValues;
        public Toggle toggleFieldPrefab;

        void Awake()
        {
            formValues = new Dictionary<string, string>();
        }
    }
}