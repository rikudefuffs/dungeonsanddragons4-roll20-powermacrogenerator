/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 27/10/2019 23:43:52
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace DnDGenerator
{
    ///<summary>
    /// A Row of the PowerVisualizer, representing a property of the power
    ///</summary>
    public class VisualizerRow : MonoBehaviour
    {
        public TMP_Text lblProperty;
        public Image sprBackground;

        public void Initialize(Color32 backgroundColor, string text)
        {
            sprBackground.color = backgroundColor;
            lblProperty.text = text;
        }
    }
}