/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 22/03/2020 21:38:34
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using SmartMVC;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SimpleJSON;

namespace DnDGenerator
{
    ///<summary>
    ///A dynamically generated Form in which the user can input data to generate a macro
    ///</summary>
    public class DynamicFormView : View<GameApplication>
    {
        GeneratorModel generatorModel { get { return app.model.generator; } }

        public RectTransform gridControls;

        TMP_InputField inputCustomField;
        TMP_Text lblCustomFieldPrefab { get { return app.view.generator.lblCustomFieldPrefab; } }
        TMP_InputField inputCustomFieldPrefab { get { return app.view.generator.inputCustomFieldPrefab; } }
        Toggle toggleFieldPrefab { get { return app.model.form.toggleFieldPrefab; } }

        public void DestroyPreviousGridControls()
        {
            gridControls.DetachChildren();
        }

        public void AddStandardProperty(string name, JSONNode structure, FormElement.NotifyAction OnValueChanged = null, System.Action<object> onButtonClickCallback = null)
        {
            AddProperty(generatorModel.standardProperties, name, structure, OnValueChanged, onButtonClickCallback);
        }

        public void AddCustomProperty(string name, JSONNode structure, FormElement.NotifyAction OnValueChanged = null, bool removable = false)
        {
            if (removable)
            {
                AddProperty(generatorModel.customProperties, name, structure, OnValueChanged, delegate { OnClickRemoveCustomProperty(name); });
                return;
            }
            AddProperty(generatorModel.customProperties, name, structure, OnValueChanged, null);
        }

        void AddProperty(Dictionary<string, FormElement> storage, string name, JSONNode structure, FormElement.NotifyAction OnValueChanged, System.Action<object> onButtonClickCallback)
        {
            if (name.IsNullOrEmpty()) { return; }
            if (storage.ContainsKey(name)) { return; }

            string propertyType = structure["type"].Value;

            TMP_Text lblInstance;
            Toggle toggleInstance;
            TMP_InputField inputInstance;

            switch (propertyType)
            {
                case "toggle":
                    (lblInstance, toggleInstance) = AddCustomPropertyUI(name, structure["value"].AsBool, onButtonClickCallback);
                    storage.Add(name, new ToggleFormElement(lblInstance, toggleInstance, OnValueChanged));
                    break;

                case "text":
                default:
                    (lblInstance, inputInstance) = AddCustomPropertyUI(name, structure["value"].Value, onButtonClickCallback);
                    storage.Add(name, new TextFormElement(lblInstance, inputInstance, OnValueChanged));
                    break;
            }

            StartCoroutine(app.view.generator.WaitAndRepositionScrollbar(1));
        }

        (TMP_Text, TMP_InputField) AddCustomPropertyUI(string name, string initialValue, System.Action<string> onButtonClickCallback)
        {
            TMP_Text lblInstance = Instantiate(lblCustomFieldPrefab, app.view.generator.controlsContainer.transform);
            lblInstance.text = name;
            lblInstance.GetComponent<LocalizeText>().key = name;
            TMP_InputField inputInstance = Instantiate(inputCustomFieldPrefab, app.view.generator.controlsContainer.transform);

            if (onButtonClickCallback == null)
            {
                Destroy(inputInstance.GetComponentInChildren<Button>().gameObject);
            }
            else
            {
                inputInstance.GetComponentInChildren<Button>().onClick.AddListener(delegate
                {
                    onButtonClickCallback(name);
                });
            }

            if (!initialValue.IsNullOrEmpty())
            {
                inputInstance.text = initialValue;
            }
            return (lblInstance, inputInstance);
        }

        (TMP_Text, Toggle) AddCustomPropertyUI(string name, bool initialValue, System.Action<object> onButtonClickCallback)
        {
            TMP_Text lblInstance = Instantiate(lblCustomFieldPrefab, app.view.generator.controlsContainer.transform);
            lblInstance.text = name;
            lblInstance.GetComponent<LocalizeText>().key = name;
            Toggle toggle = Instantiate(toggleFieldPrefab, app.view.generator.controlsContainer.transform);
            toggle.isOn = initialValue;
            if (onButtonClickCallback != null)
            {
                toggle.onValueChanged.AddListener(delegate { onButtonClickCallback(toggle.isOn); });
            }
            return (lblInstance, toggle);
        }

        public void AddCustomPropertyInputBox()
        {
            (TMP_Text lblInstance, TMP_InputField inputInstance) = AddCustomPropertyUI("Add Custom Field", string.Empty, OnClickAddCustomProperty);
            inputCustomField = inputInstance;
            Button btnAdd = inputInstance.GetComponentInChildren<Button>();
            btnAdd.GetComponentInChildren<TMP_Text>().text = "+";
            ColorBlock btnColors = btnAdd.colors;
            btnColors.normalColor = new Color32(50, 112, 56, 255);
            btnColors.highlightedColor = new Color32(40, 102, 46, 255);
            btnAdd.colors = btnColors;
        }

        void OnClickAddCustomProperty(string IGNORED)
        {
            JSONNode newPropertyData = JSONNode.Parse("{}");
            newPropertyData.Add("type", "text");
            newPropertyData.Add("value", "");

            JSONNode newPropertyStructure = JSONNode.Parse("{}");
            newPropertyStructure.Add(inputCustomField.text, newPropertyData);

            AddCustomProperty(inputCustomField.text, newPropertyStructure, null, true);
            EventSystem.current.SetSelectedGameObject(inputCustomField.gameObject);
            inputCustomField.text = string.Empty;
        }
        void OnClickRemoveCustomProperty(string fieldName) { RemoveCustomProperty(fieldName); }

        void RemoveCustomProperty(string name)
        {
            if (!generatorModel.customProperties.ContainsKey(name)) { return; }
            generatorModel.RemoveCustomProperty(name);
        }

        public void RemoveElement(string elementID)
        {
            if (!generatorModel.customProperties.ContainsKey(elementID)) { return; }
            generatorModel.RemoveCustomProperty(elementID);
        }
    }
}