/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 25/10/2019 15:37:00
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
using System.Linq;
using TMPro;
using UnityEngine.EventSystems;
using SmartMVC;
using SimpleJSON;
using System.IO;

namespace DnDGenerator
{
    ///<summary>
    ///Handles the UI of the Generator
    ///</summary>

    public class GeneratorView : View<GameApplication>
    {
        GeneratorModel generatorModel { get { return app.model.generator; } }
        Coroutine messageRoutine;

        public TMP_InputField inputIndex;
        public TMP_Dropdown lstUsage;
        public Scrollbar scrGenerator;
        public TMP_Text lblLanguage;
        public GridLayoutGroup controlsContainer;

        [Header("Keywords")]
        public TMP_InputField inputKeyword;
        public Toggle keywordTogglePrefab;
        public GridLayoutGroup keywordTogglesContainer;
        public Scrollbar scrKeywords;
        public Button btnOpenKeywordsContainer;

        [Header("Target")]
        public TMP_Dropdown lstTarget;

        [Header("Controls")]
        public Button btnReset;
        public Button btnGenerate;
        public Button btnGenerateAndReset;
        public Button btnGenerateAndGoToNext;
        public Button btnSaveToTemplate;
        public Button btnLoadFromTemplate;
        public Button btnSwitchEditor;

        [Header("Macro Output")]
        public TMP_InputField inputMacro;
        public TMP_Text lblCopiedToClipboard;

        [Header("Attack Roll")]
        public TMP_Dropdown lstAttackRollDefenses;
        public TMP_Dropdown lstDamageTypes;
        public TMP_InputField inputAdditionalHitEffect;
        public Toggle tglUseMiscOnly;

        [Header("Effect")]
        public TMP_InputField inputEffect;

        [Header("Miss")]
        public TMP_InputField inputMissEffect;

        [Header("Custom Fields")]
        public TMP_InputField inputCustomField;
        public Button btnAddCustomField;
        public TMP_Text lblCustomFieldPrefab;
        public TMP_InputField inputCustomFieldPrefab;

        [Header("Flavor and utilities")]
        public TMP_InputField inputLevel;
        public TMP_InputField inputEmote;
        public TMP_InputField inputLink;
        public PowerVisualizer powerVisualizer;
        public Button btnOptions;
        public Button btnQuit;

        void Start()
        {
            btnReset.onClick.AddListener(OnClickReset);
            btnGenerate.onClick.AddListener(OnClickGenerate);
            btnGenerateAndReset.onClick.AddListener(OnClickGenerateAndReset);
            btnGenerateAndGoToNext.onClick.AddListener(OnClickGenerateAndGoToNext);
            btnSaveToTemplate.onClick.AddListener(OnClickSaveToTemplate);
            btnLoadFromTemplate.onClick.AddListener(OnClickLoadFromTemplate);
            btnSwitchEditor.onClick.AddListener(OnClickSwitchEditor);

            if (generatorModel.isGeneratingPower)
            {
                btnOpenKeywordsContainer.onClick.AddListener(OnClickToggleKeywordsContainer);
                btnAddCustomField.onClick.AddListener(OnClickAddCustomField);
                btnOptions.onClick.AddListener(OnClickOpenOptions);
                btnQuit.onClick.AddListener(OnClickQuit);

                inputIndex.onValueChanged.AddListener(OnIndexChanged);
                inputMacro.onFocusSelectAll = true;
                inputMacro.onSelect.AddListener(OnMacroBoxClicked);
                inputAdditionalHitEffect.onValueChanged.AddListener(OnAdditionalHitEffectChanged);
                inputEffect.onValueChanged.AddListener(OnEffectChanged);
                inputMissEffect.onValueChanged.AddListener(OnMissEffectChanged);
                inputEmote.onValueChanged.AddListener(OnEmoteChanged);
                inputLevel.onValueChanged.AddListener(OnLevelChanged);
                inputLink.onValueChanged.AddListener(OnLinkChanged);
                tglUseMiscOnly.onValueChanged.AddListener(OnMiscValuePreferenceChanged);
            }

            lblCopiedToClipboard.gameObject.SetActive(false);

            ResetUI();
        }

        /// <summary>
        /// Enables all the toggles toggle whose keyword is selected
        /// </summary>
        /// <param name="selectedkeywords"></param>
        public void ForceKeywordsToggleUIOn(List<string> selectedkeywords)
        {
            int childrenCount = keywordTogglesContainer.transform.childCount;
            for (int i = 0; i < childrenCount; i++)
            {
                Toggle toggle = keywordTogglesContainer.transform.GetChild(i).GetComponent<Toggle>();
                if (!selectedkeywords.Contains(toggle.GetComponentInChildren<LocalizeText>().key)) { continue; }
                toggle.SetIsOnWithoutNotify(true);
            }
        }

        void SetupKeywordsList()
        {
            UIUtilities.ClearContainer(keywordTogglesContainer);

            foreach (string keyword in GeneratorModel.keywords)
            {
                SpawnKeywordToggle(keyword);
            }
            scrKeywords.SetValueWithoutNotify(1);
        }

        void SpawnKeywordToggle(string keyword)
        {
            Toggle toggleInstance = Instantiate(keywordTogglePrefab, keywordTogglesContainer.transform);
            toggleInstance.GetComponentInChildren<LocalizeText>().key = keyword;
            toggleInstance.GetComponentInChildren<LocalizeText>().OnLocalize();
            toggleInstance.onValueChanged.AddListener(delegate { OnKeywordToggleValueChanged(toggleInstance, keyword); });
        }

        void OnKeywordToggleValueChanged(Toggle t, string keyword)
        {
            if (t.isOn)
            {
                OnKeywordAdded(keyword);
                return;
            }
            OnKeywordRemoved(keyword);
        }

        void OnClickRemoveCustomProperty(string propertyName) { RemoveCustomProperty(propertyName); }

        void SetupUsageDropdown()
        {
            lstUsage.ClearOptions();
            var values = System.Enum.GetValues(typeof(PowerUsage)).Cast<PowerUsage>().Select(v => v.ToDescriptionString()).ToList();
            lstUsage.AddOptions(values);
            lstUsage.onValueChanged.RemoveListener(OnUsageTypeChanged);
            lstUsage.onValueChanged.AddListener(OnUsageTypeChanged);
        }

        void SetupTargetDropdown()
        {
            lstTarget.ClearOptions();
            lstTarget.AddOptions(GeneratorModel.standardTargets.ToList());
            lstTarget.onValueChanged.RemoveListener(OnTargetChanged);
            lstTarget.onValueChanged.AddListener(OnTargetChanged);
        }

        void SetupAttackRollDropdown()
        {
            lstAttackRollDefenses.ClearOptions();
            lstAttackRollDefenses.AddOptions(GeneratorModel.attackRollsDefenses.ToList());
            lstAttackRollDefenses.onValueChanged.RemoveListener(OnAttackRollDefenseChanged);
            lstAttackRollDefenses.onValueChanged.AddListener(OnAttackRollDefenseChanged);

            lstDamageTypes.ClearOptions();
            var values = System.Enum.GetValues(typeof(DamageType)).Cast<DamageType>().Select(v => v.ToDescriptionString()).ToList();
            lstDamageTypes.AddOptions(values);
            lstDamageTypes.onValueChanged.RemoveListener(OnDamageTypeChanged);
            lstDamageTypes.onValueChanged.AddListener(OnDamageTypeChanged);
        }

        void OnMacroBoxClicked(string selectedText)
        {
            selectedText.CopyToClipboard();
            OnMacroCopied();
        }

        void OnIndexChanged(string newIndex)
        {
            if (newIndex.IsNullOrEmpty()) { return; }
            generatorModel.powerIndex = System.Convert.ToInt32(newIndex);
        }

        void OnAdditionalHitEffectChanged(string effect) { generatorModel.additionalHitEffect = effect; }
        void OnEffectChanged(string effect) { generatorModel.effect = effect; }
        void OnMissEffectChanged(string effect) { generatorModel.missEffect = effect; }
        void OnEmoteChanged(string effect) { generatorModel.emote = effect; }
        void OnLevelChanged(string effect) { generatorModel.level = effect; }
        void OnLinkChanged(string effect) { generatorModel.link = effect; }
        void OnMiscValuePreferenceChanged(bool value) { generatorModel.useOnlyMiscValueForDamageAndAttackRolls = value; }

        void OnUsageTypeChanged(int index) { generatorModel.powerUsage = (PowerUsage)lstUsage.value; }
        void OnTargetChanged(int index)
        {
            generatorModel.powerTargetIndex = index;
        }
        void OnAttackRollDefenseChanged(int index) { generatorModel.defenseFromAttackRollIndex = index; }
        void OnDamageTypeChanged(int index) { generatorModel.damageRollType = (DamageType)lstDamageTypes.value; }

        public void ForceValuesUpdate()
        {
            OnIndexChanged(inputIndex.text);
            OnAdditionalHitEffectChanged(inputAdditionalHitEffect.text);
            OnEffectChanged(inputEffect.text);
            OnMissEffectChanged(inputMissEffect.text);
            OnEmoteChanged(inputEmote.text);
            OnLevelChanged(inputLevel.text);
            OnLinkChanged(inputLink.text);
            OnMiscValuePreferenceChanged(tglUseMiscOnly.isOn);
            UpdateKeywordsUI();
        }

        void OnKeywordRemoved(string keyword)
        {
            if (!generatorModel.powerKeywords.Contains(keyword)) { return; }
            generatorModel.powerKeywords.Remove(keyword);
            UpdateKeywordsUI();
        }

        void OnKeywordAdded(string keyword)
        {
            if (generatorModel.powerKeywords.Contains(keyword)) { return; }
            generatorModel.powerKeywords.Add(keyword);
            UpdateKeywordsUI();
        }

        void UpdateKeywordsUI()
        {
            string[] localizedKeywords = generatorModel.powerKeywords.Select(s => Localization.Get(s)).ToArray();
            inputKeyword.SetTextWithoutNotify(string.Join(", ", localizedKeywords));
        }

        void OnClickToggleKeywordsContainer()
        {
            scrKeywords.transform.parent.gameObject.SetActive(!scrKeywords.transform.parent.gameObject.activeSelf);
        }

        public void AddCustomProperty(string name, string initialValue = "")
        {
            if (name.IsNullOrEmpty()) { return; }
            if (generatorModel.customProperties.ContainsKey(name)) { return; }
            TMP_Text lblInstance = Instantiate(lblCustomFieldPrefab, controlsContainer.transform);
            lblInstance.text = name;
            TMP_InputField inputInstance = Instantiate(inputCustomFieldPrefab, controlsContainer.transform);
            inputInstance.GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                OnClickRemoveCustomProperty(name);
            });
            if (!initialValue.IsNullOrEmpty())
            {
                inputInstance.text = initialValue;
            }
            generatorModel.customProperties.Add(name, new TextFormElement(lblInstance, inputInstance, null));
            EventSystem.current.SetSelectedGameObject(inputInstance.gameObject);
            inputCustomField.text = string.Empty;
            StartCoroutine(WaitAndRepositionScrollbar(0));
        }

        public IEnumerator WaitAndRepositionScrollbar(float newValue)
        {
            yield return CoroutinesHelper.EndOfFrame;
            scrGenerator.onValueChanged.Invoke(newValue);
        }

        void RemoveCustomProperty(string name)
        {
            if (!generatorModel.customProperties.ContainsKey(name)) { return; }
            generatorModel.RemoveCustomProperty(name);
        }

        public void ResetUI()
        {
            inputIndex.text = "1";
            inputKeyword.text = string.Empty;

            SetupUsageDropdown();
            SetupKeywordsList();
            SetupTargetDropdown();
            SetupAttackRollDropdown();

            UIUtilities.ResetDropdown(lstUsage, 0);
            UIUtilities.ResetDropdown(lstTarget, 0);
            UIUtilities.ResetDropdown(lstAttackRollDefenses, 0);
            UIUtilities.ResetDropdown(lstDamageTypes, 0);

            inputAdditionalHitEffect.text = string.Empty;
            inputEffect.text = string.Empty;
            inputMissEffect.text = string.Empty;
            inputEmote.text = string.Empty;
            inputLevel.text = string.Empty;
            inputLink.text = string.Empty;
            inputCustomField.text = string.Empty;

            tglUseMiscOnly.isOn = false;
            tglUseMiscOnly.onValueChanged.Invoke(false);

            scrGenerator.onValueChanged.Invoke(1);
            ForceValuesUpdate();
        }

        public void OnClickAddCustomField() { AddCustomProperty(inputCustomField.text); }
        public void OnClickQuit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        public void OnClickOpenOptions() { Notify(MVCEvents.OPEN_OPTIONS); }
        public void OnClickReset() { Notify(MVCEvents.GENERATOR_RESET); }
        public void OnClickGenerate() { Notify(MVCEvents.GENERATE_MACRO, generatorModel.isGeneratingPower); }
        public void OnClickGenerateAndReset() { OnClickGenerate(); OnClickReset(); }
        public void OnClickGenerateAndGoToNext()
        {
            OnClickGenerate();
            inputIndex.text = (generatorModel.powerIndex + 1).ToString();
            OnIndexChanged(inputIndex.text);
        }
        public void OnClickSaveToTemplate()
        {
            string path = SFB.StandaloneFileBrowser.SaveFilePanel(Localization.Get("ChooseExportFolder"), "", "", "json");
            if (path.Length == 0) { return; }
            if (string.IsNullOrEmpty(path)) { return; }
            Notify(MVCEvents.GENERATOR_SAVE, path);
        }

        public void OnClickLoadFromTemplate()
        {
            string[] path = SFB.StandaloneFileBrowser.OpenFilePanel(Localization.Get("ChooseSetToImport"), "", "json", false);
            if (path.Length == 0) { return; }
            if (string.IsNullOrEmpty(path[0])) { return; }
            Notify(MVCEvents.GENERATOR_LOAD, path[0]);
        }

        public void OnClickSwitchEditor()
        {
            generatorModel.isGeneratingPower = !generatorModel.isGeneratingPower;
            if (generatorModel.isGeneratingPower) { return; }
            JSONNode form = JSONUtilities.ReadJSONFromFile(Path.Combine(Application.streamingAssetsPath, "Form_Ritual.json"));
            Notify(MVCEvents.FORM_LOAD, form);
        }

        public void OnMacroCopied()
        {
            CoroutinesHelper.StopAndNullifyRoutine(ref messageRoutine, this);
            messageRoutine = StartCoroutine(ShowCopiedMessage());
        }
        IEnumerator ShowCopiedMessage()
        {
            lblCopiedToClipboard.gameObject.SetActive(true);
            yield return CoroutinesHelper.ThreeSeconds;
            lblCopiedToClipboard.gameObject.SetActive(false);
        }
    }
}