/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 28/10/2019 10:46:33
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SmartMVC;

namespace DnDGenerator
{
    ///<summary>
    ///Handles the UI of the Settngs window
    ///</summary>
    public class SettingsView : View<GameApplication>
    {
        public Toggle tglFullscreen;
        public Button btnSeeLatestVersions;
        public Button btnContribute;
        public Button btnDonate;
        public Button btnBack;

        void Start()
        {
            tglFullscreen.SetIsOnWithoutNotify(Screen.fullScreen);
            tglFullscreen.onValueChanged.AddListener(OnFullscreenToggleValueChanged);
            btnDonate.onClick.AddListener(OnClickDonate);
            btnContribute.onClick.AddListener(OnClicContribute);
            //btnSeeLatestVersions.onClick.AddListener(OnClicSeeLatestVersion);
            btnBack.onClick.AddListener(OnClickBack);
        }

        void OnFullscreenToggleValueChanged(bool isOn)
        {
            Screen.fullScreen = isOn;
        }

        void OnClickBack() { gameObject.SetActive(false);}
        void OnClickDonate() { Application.OpenURL("https://www.paypal.com/donate/?token=tBPuPNpeTaeDmJaosB-4hcE0_6nizOlkuP53I20_O4GP3pPR6A3fdMp5_wGv3aY2V8LeGm&country.x=IT&locale.x=IT"); }
        void OnClicSeeLatestVersion() { Application.OpenURL("https://gitlab.com/rikudefuffs/dungeonsanddragons4-roll20-powermacrogenerator"); }
        void OnClicContribute() { Application.OpenURL("https://gitlab.com/rikudefuffs/dungeonsanddragons4-roll20-powermacrogenerator"); }
    }
}