/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 24/10/2019 20:55:24
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using System.Text;
using System.Linq;
using System.Collections.Generic;
namespace DnDGenerator
{
    ///<summary>
    ///Generates Powers' macros
    ///</summary>
    public static class PowerGenerator
    {
        public static string GenerateAttack(string propertyPrefix, bool miscModifierOnly)
        {
            return "[[1d20+@{" + propertyPrefix + (miscModifierOnly ? "attack-misc}]] vs **@{" : "attack}]] vs **@{") + propertyPrefix + "def}**";
        }

        public static string GenerateHit(string propertyPrefix, DamageRollData data)
        {
            bool miscModifierOnly = data.useOnlyMiscModifier;
            string damageType = data.damageType.ToDescriptionString();

            if (!data.doesDamageRoll)
            {
                return data.flatDamage + GenerateLocalizedDamageSentence(damageType) + (!data.additionalHitEffect.IsNullOrEmpty() ? (" " + data.additionalHitEffect) : "");
            }

            return "[[@{" + propertyPrefix + "weapon-num-dice}d@{" + propertyPrefix + "weapon-dice}+@{" + propertyPrefix + "damage" + (miscModifierOnly ? "-misc}]]" : "}]]")
                + GenerateLocalizedDamageSentence(damageType) + (!data.additionalHitEffect.IsNullOrEmpty() ? (" " + data.additionalHitEffect) : "");

        }

        public static string GenerateEmote(string emoteText) { return "@{character_name} " + emoteText; }
        public static string GenerateLevel(string propertyPrefix, string className) { return className + " @{" + propertyPrefix + "level}"; }
        public static string GenerateLink(string link) { return string.Format("{0}({1})", Localization.Get("LinkInMacro"), link); }
        public static string GenerateRitualRoll(string skill, string characterName) { return "%{" + characterName + "|-" + skill.ToLower() + "-check}"; }


        static string GenerateLocalizedDamageSentence(string damageType)
        {
            switch (Localization.language)
            {
                case "Italiano":
                    if (damageType.IsNullOrEmpty())
                    {
                        return string.Format(" Danni");
                    }
                    return string.Format(" Danni da {0}", Localization.Get(damageType));
                case "English":
                    if (damageType.IsNullOrEmpty())
                    {
                        return string.Format(" Damage");
                    }
                    return string.Format(" {0} Damage", Localization.Get(damageType));
            }
            return string.Empty;
        }

        public static Power GenerateAndReturn(int powerIndex, PowerUsage powerUsage, string[] keywords, string target, Dictionary<string, object> additionalProperties)
        {
            Power power = new Power(powerIndex, powerUsage, keywords, target);
            power.AddProperties(additionalProperties);
            return power;
        }

        public static Ritual GenerateAndReturn(int index, Dictionary<string, object> additionalProperties)
        {
            Ritual ritual = new Ritual(index);
            ritual.AddProperties(additionalProperties);
            return ritual;
        }

        public static string Generate(int powerIndex, PowerUsage powerUsage, string[] keywords, string target, Dictionary<string, object> additionalProperties)
        {
            Power power = new Power(powerIndex, powerUsage, keywords, target);
            power.AddProperties(additionalProperties);
            return power.ToString();
        }
    }
}