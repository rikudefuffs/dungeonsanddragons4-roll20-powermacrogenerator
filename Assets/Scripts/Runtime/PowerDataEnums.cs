/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 25/10/2019 15:58:09
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using System.ComponentModel;
namespace DnDGenerator
{
    ///<summary>
    ///A collection of all the enums that can be used for parts of a power
    ///</summary>

    public static class PowerDataEnums
    {
        public static string ToDescriptionString(this PowerUsage val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }

        public static string ToDescriptionString(this DamageType val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }

    public enum PowerUsage
    {
        [Description("atwill")]
        AtWill,
        [Description("encounter")]
        Encounter,
        [Description("daily")]
        Daily,
        [Description("item")]
        Item,
        [Description("ability")]
        Ability,
        [Description("skill")]
        Skill,
        [Description("other")]
        Other
    }

    public enum DamageType
    {
        [Description("")]
        Untyped,
        [Description("Fire")]
        Fire,
        [Description("Necrotic")]
        Necrotic,
        [Description("Cold")]
        Cold,
        [Description("Radiant")]
        Radiant,
        [Description("Acid")]
        Acid,
        [Description("Force")]
        Force,
        [Description("Lightning")]
        Lightning,
        [Description("Poison")]
        Poison,
        [Description("Psychic")]
        Psychic,
        [Description("Thunder")]
        Thunder,
    }

    public struct AttackRollData
    {
        public string defense;
        public bool useOnlyMiscModifier;

        public AttackRollData(string defense, bool useOnlyMiscModifier)
        {
            this.defense = defense;
            this.useOnlyMiscModifier = useOnlyMiscModifier;
        }
    }

    public struct DamageRollData
    {
        public bool doesDamageRoll { get { return flatDamage == 0; } }
        public bool useOnlyMiscModifier;
        public DamageType damageType;
        /// <summary>
        /// For when no damage roll is required
        /// </summary>
        public int flatDamage;
        public string additionalHitEffect;

        public DamageRollData(bool useOnlyMiscModifier, DamageType damageType, int flatDamage = 0, string additionalHitEffect = "")
        {
            this.useOnlyMiscModifier = useOnlyMiscModifier;
            this.damageType = damageType;
            this.flatDamage = flatDamage;
            this.additionalHitEffect = additionalHitEffect;
        }
    }
}