/* ----------------------------------------------
 * 
 * 				Hunt For Gods
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: 12/7/2017 14:50:04 PM
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StarworkMath
{
    public static int Modulo(int value, int lenght)
    {
        return ((value % lenght) + lenght) % lenght;
    }

    /// <summary>
    /// Returns the angle between two screen points
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="pointB"></param>
    /// <returns></returns>
    public static float GetScreenPointsAngle(Vector3 origin, Vector3 pointB)
    {
        return Mathf.Abs((Mathf.Atan2(origin.y - pointB.y, origin.x - pointB.x) * 180 / Mathf.PI) - 180);
    }

    /// <summary>
    /// Roudns a float value to a 2 digit float value (I.E: 0.578494 to 0.57)
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static float RoundFloatToTwoDigits(float value)
    {
        return (Mathf.Round(value * 100f) / 100f);
    }

    /// <summary>
    /// Maps a value from one range to another
    /// </summary>
    /// <param name="value">The value you want to map</param>
    /// <param name="fromSource">The starting point of the current value range</param>
    /// <param name="toSource">The end point of the current value range</param>
    /// <param name="fromTarget">The starting point of the desired value range</param>
    /// <param name="toTarget">The end point of the desired value range</param>
    /// <returns></returns>
    public static float Map(float value, float fromSource, float toSource, float fromTarget, float toTarget)
    {
        return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
    }

    /// <summary>
    /// Removes all the floating errors from the floats of the JSON
    /// </summary>
    /// <param name="jsonWithFloats"></param>
    public static void RemoveFloatingErrorsFrom(SimpleJSON.JSONNode jsonWithFloats)
    {
        foreach (string key in jsonWithFloats.Keys)
        {
            float valueToSanitize = jsonWithFloats[key].AsFloat;
            if (valueToSanitize == 0.0f) { /*Debug.LogError("Not a float: " + jsonWithFloats[key].Value);*/ continue; }
            valueToSanitize = (valueToSanitize * 100f) / 100f;
            jsonWithFloats[key].AsFloat = valueToSanitize;
        }
    }

    /// <summary>
    /// Möller-Trumbore Algorithm
    /// is a fast method for calculating the intersection of a ray and a triangle in three dimensions 
    /// without needing precomputation of the plane equation of the plane containing the triangle
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="p3"></param>
    /// <param name="ray"></param>
    /// <returns>True if the ray intersects with the triangle</returns>
    public static bool DoesRayIntersectWithTriangles(Vector3 p1, Vector3 p2, Vector3 p3, Ray ray)
    {
        // Vectors from p1 to p2/p3 (edges)
        Vector3 e1, e2;

        Vector3 p, q, t;
        float det, invDet, u, v;

        //Find vectors for two edges sharing vertex/point p1
        e1 = p2 - p1;
        e2 = p3 - p1;

        // calculating determinant
        p = Vector3.Cross(ray.direction, e2);

        //Calculate determinat
        det = Vector3.Dot(e1, p);

        //if determinant is near zero, ray lies in plane of triangle otherwise not
        if (det > -Mathf.Epsilon && det < Mathf.Epsilon) { return false; }
        invDet = 1.0f / det;

        //calculate distance from p1 to ray origin
        t = ray.origin - p1;

        //Calculate u parameter
        u = Vector3.Dot(t, p) * invDet;

        //Check for ray hit
        if (u < 0 || u > 1) { return false; }

        //Prepare to test v parameter
        q = Vector3.Cross(t, e1);

        //Calculate v parameter
        v = Vector3.Dot(ray.direction, q) * invDet;

        //Check for ray hit
        if (v < 0 || u + v > 1) { return false; }

        if ((Vector3.Dot(e2, q) * invDet) > Mathf.Epsilon)
        {
            //ray does intersect
            return true;
        }

        // No hit at all
        return false;
    }

    /// <summary>
    /// Calculates the direction vector for repellingan object from another
    /// </summary>
    /// <param name="repelledObject"></param>
    /// <param name="repeller"></param>
    /// <returns></returns>
    public static Vector3 GetRepulsionDirectionFromObject(Transform repelledObject, Transform repeller)
    {
        return (repelledObject.localPosition - repeller.localPosition).normalized;
    }

    /// <summary>
    /// Faster way to calculate distance between 2 points, without using the slow SQRT Math function
    /// </summary>
    /// <param name="pointA"></param>
    /// <param name="pointB"></param>
    /// <returns>The squared distance between the two points</returns>
    public static float CalculateSquaredDistance(Vector3 pointA, Vector3 pointB)
    {
        return (pointA - pointB).sqrMagnitude;
    }

    /// <summary>
    /// Returns true if the distance between two points is within the specified max distance
    /// </summary>
    /// <param name="pointA"></param>
    /// <param name="pointB"></param>
    /// <param name="squaredMaxDistance"></param>
    /// <returns></returns>
    public static bool IsDistanceWithinMaxDistance(Vector3 pointA, Vector3 pointB, float squaredMaxDistance)
    {
        return CalculateSquaredDistance(pointA, pointB) < squaredMaxDistance;
    }
}
