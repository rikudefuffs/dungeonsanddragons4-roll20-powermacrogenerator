/* ----------------------------------------------
 * 
 * 				Hunt For Gods
 * 
 * Original Author: Abela Paolo
 * Creation Date: 4/12/2019 12:11:01 PM
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;

///<summary>
///A class with a lot of extension methods and utilities that we can use in every project
///</summary>
public static class StarworkUtilities
{
    public static bool IsNullOrEmpty(this ICollection collection)
    {
        return ((collection == null) || (collection.Count < 1));
    }

    public static bool IsNullOrEmpty<T>(this T[] array)
    {
        return ((array == null) || (array.Length < 1));
    }

    public static bool IsNullOrEmpty(this string str)
    {
        return string.IsNullOrEmpty(str);
    }

    /// <summary>
    /// Prints out a vector3 with only 1 decimal
    /// </summary>
    /// <param name="vector"></param>
    /// <returns></returns>
    public static string ToDetailedString(this Vector3 vector)
    {
        return string.Format("{0},{1},{2}", vector.x.ToString(), vector.y.ToString(), vector.z.ToString());
    }

    /// <summary>
    /// There is a really easy way you can remap a progressing float to an arbitrary float array, taking a lesson from color LUTs
    /// </summary>
    /// <param name="input"></param>
    /// <param name="floatTable"></param>
    /// <returns></returns>
    public static float RemapToStandard(float input, float[] floatTable)
    {
        //I.E of floatTable = new float[] {0.9f, 1.3f, 1.2f, 0.8f};
        int inputInt = Mathf.FloorToInt(input);
        return Mathf.Lerp(floatTable[inputInt], floatTable[inputInt + 1], input - inputInt);
    }

    /// <summary>
    /// Copies a string to the clipboard of the computer, so you can paste it somewhere
    /// </summary>
    /// <param name="s"></param>
    public static void CopyToClipboard(this string s)
    {
        TextEditor te = new TextEditor();
        te.text = s;
        te.SelectAll();
        te.Copy();
    }
}
