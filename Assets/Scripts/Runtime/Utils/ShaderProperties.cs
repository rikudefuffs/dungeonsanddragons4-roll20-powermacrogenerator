/* ----------------------------------------------
 * 
 * 				Hunt For Gods
 * 
 * Original Author: Abela Paolo
 * Creation Date: 11/9/2018 10:04:20 AM
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;

///<summary>
/// Contains all the properties you might need for shaders
///</summary>
public static class ShaderProperties
{
    #region Common
    public static readonly int tintColor = Shader.PropertyToID("_TintColor");
    public static readonly int color = Shader.PropertyToID("_Color");
    public static readonly int emissionColor = Shader.PropertyToID("_EmissionColor");
    public static readonly int mainTexture = Shader.PropertyToID("_MainTex");
    public static readonly int bumpTex = Shader.PropertyToID("_BumpTex");
    public static readonly int cutoff = Shader.PropertyToID("_Cutoff");
    #endregion

    #region Krypto
    public static readonly int texNextFrame = Shader.PropertyToID("_Tex_NextFrame");
    public static readonly int interpolationValue = Shader.PropertyToID("InterpolationValue");
    #endregion
}
