/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 26/10/2019 17:44:55
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using TMPro;

///<summary>
/// Automatically localizes a TMP Dropdown 
///</summary>

public class LocalizedTMPDropdown : TMP_Dropdown
{
    LocalizeText localizedCurrentValue;

    protected override void Awake()
    {
        base.Awake();
        localizedCurrentValue = captionText.GetComponent<LocalizeText>();
        if (localizedCurrentValue) { return; }
        localizedCurrentValue = captionText.gameObject.AddComponent<LocalizeText>();
    }
    protected override void Start()
    {
        base.Start();
        RefreshShownValue();
        onValueChanged.AddListener(OnValueChanged);
        OnValueChanged(value);

    }
    public void OnValueChanged(int index)
    {
        localizedCurrentValue.key = captionText.text;
        localizedCurrentValue.OnLocalize();
    }
}
