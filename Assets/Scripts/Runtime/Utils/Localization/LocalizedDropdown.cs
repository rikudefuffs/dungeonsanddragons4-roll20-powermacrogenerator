/* ----------------------------------------------
 * 
 * 				PacmanHackAdemy
 * 
 * Original Author: Davide Giuseppe Lepore
 * Creation Date: 2/1/2019 9:45:09 AM
 * Updates: 
 * 
 * Copyright © GamingHackAdemy
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

///<summary>
///
///</summary>
public class LocalizedDropdown : Dropdown
{
    LocalizeText localizedCurrentValue;
    [SerializeField] string prefix;

    protected override void Awake()
    {
        base.Awake();
        localizedCurrentValue = captionText.GetComponent<LocalizeText>();
    }
    protected override void Start()
    {
        base.Start();
        foreach (OptionData option in options)
        {
            if (!option.text.StartsWith(prefix))
            {
                option.text = prefix + option.text;
            }
        }
        RefreshShownValue();
        onValueChanged.AddListener(OnValueChanged);

    }
    public void OnValueChanged(int index)
    {
        if (captionText.text.StartsWith(prefix))
        {
            localizedCurrentValue.key = captionText.text;
        }
        else
        {
            localizedCurrentValue.key = prefix + captionText.text;
        }
        localizedCurrentValue.OnLocalize();
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        foreach (OptionData option in options)
        {
            if (!option.text.StartsWith(prefix))
            {
                option.text = prefix + option.text;
            }
        }
    }
}