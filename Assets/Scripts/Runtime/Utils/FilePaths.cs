/* ----------------------------------------------
 * 
 *     Hunt For Gods
 * 
 * Original Author: Abela Paolo
 * Creation Date: 12 November 2015
 * 
 * Copyright © 2015-2016 Starwork Game Creators
 * ----------------------------------------------
 */

using UnityEngine;
using System.Collections;

/// <summary>
/// Contains references to all the paths used to load files at runtime
/// </summary>
public static class FilePaths
{
    public const string OPTIONS_File = "Options.txt";

    /// <summary>
    /// Root folder of the Configuration files
    /// </summary>
    public readonly static string CONFIGURATION_Root = Application.streamingAssetsPath + "/Configuration/";
    /// <summary>
    /// Files which contains all the usable keycodes
    /// </summary>
    public static readonly string CONFIGURATION_KeycodeList = CONFIGURATION_Root + "KeycodeList.txt";
    /// <summary>
    /// Files which contains the standard configuration
    /// </summary>
    public static readonly string CONFIGURATION_DefaultKeys = CONFIGURATION_Root + "GameOptions.txt.example";

    /// <summary>
    /// Game Options which contains the current configuration about video, music ad input settings
    /// </summary>
    public const string GameOptions_File = "GameOptions.txt";
}