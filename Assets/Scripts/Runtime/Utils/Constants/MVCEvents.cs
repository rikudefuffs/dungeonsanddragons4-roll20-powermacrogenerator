/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Original Author: Abela Paolo
 * Creation Date: 12/31/2016 10:46:50 AM
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */

///<summary>
/// Contains all the AMVCC events that can be notified
///</summary>

public class MVCEvents
{
    public const int GENERATE_MACRO = 0;
    public const int GENERATOR_RESET = 1;
    public const int GENERATOR_SAVE = 2;
    public const int GENERATOR_LOAD = 3;
    public const int OPEN_OPTIONS = 4;
    public const int FORM_LOAD = 5;
    public const int FORM_INDEX_CHANGED = 6;
    public const int FORM_EMOTE_CHANGED = 7;
    public const int FORM_LEVEL_CHANGED = 8;
    public const int FORM_LINK_CHANGED = 9;
    public const int FORM_USEAGE_CHANGED = 10;
    public const int FORM_FORCE_UPDATE = 11;
    public const int FORM_ADD_RITUAL_ROLL_CHANGED = 12;
    public const int FORM_CHARACTER_OF_RITUAL_ROLL_CHANGED = 13;
}