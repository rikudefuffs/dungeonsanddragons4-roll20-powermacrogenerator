/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 10/04/2020 13:30:16
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SceneDirtyFlagChecker : MonoBehaviour
{
#if UNITY_EDITOR
    private static bool sceneIsDirty;

    static SceneDirtyFlagChecker()
    {
        Undo.postprocessModifications += OnPostProcessModifications;
    }

    private static UndoPropertyModification[] OnPostProcessModifications(UndoPropertyModification[] propertyModifications)
    {
        sceneIsDirty = true;
        Debug.LogWarning($"Scene was marked Dirty by number of objects = {propertyModifications.Length}");
        for (int i = 0; i < propertyModifications.Length; i++)
        {
            Debug.LogWarning($"currentValue.target = {propertyModifications[i].currentValue.target}", propertyModifications[i].currentValue.target);
        }
        return propertyModifications;
    }
#endif
}