/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 26/10/2019 18:15:16
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

///<summary>
///A collection of UI utilities
///</summary>

public class UIUtilities
{
    public static void ResetDropdown(TMP_Dropdown dropdown, int startValue)
    {
        dropdown.value = startValue;
        dropdown.onValueChanged.Invoke(startValue);
    }

    public static void ClearContainer(LayoutGroup layoutGroup, params Transform[] transformsToIgnore)
    {
        Transform cachedContainerTransform = layoutGroup.transform;
        int children = cachedContainerTransform.childCount;
        Transform current;
        for (int i = children - 1; i >= 0; i--)
        {
            current = cachedContainerTransform.GetChild(i);
            if (transformsToIgnore.Contains(current)) { continue; }
            GameObject.Destroy(current.gameObject);
        }
    }
}
