using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using DnDGenerator;

namespace Tests
{
    public class PowerVisualizerTests
    {
        string originalLanguage;
        [OneTimeSetUp]
        public void Setup()
        {
            originalLanguage = Localization.language;
            Localization.language = "English";
        }

        [OneTimeTearDown]
        public void Teardown()
        {
            Localization.language = originalLanguage;
        }

        [TestCase("[[1d20+150]]")]
        [TestCase("[[1d20+@{power-1-attack-misc}]]")]
        [TestCase("[[(1d20+@{power-1-attack-misc})+15]]")]
        public void ConvertExpression_AllWithinBrackets_Passes(string valueToConvert)
        {
            string result = PowerVisualizer.ConvertRoll20Expression(valueToConvert);
            int integerResult = System.Convert.ToInt32(result);
            Assert.True((integerResult > 0) && (integerResult < 100));
        }

        [TestCase("@{power-1-attack-misc}")]
        public void ConvertExpression_NoBrackets_Passes(string valueToConvert)
        {
            string result = PowerVisualizer.ConvertRoll20Expression(valueToConvert);
            int integerResult = System.Convert.ToInt32(result);
            Assert.True((integerResult > 0) && (integerResult < 100));
        }

        [Test]
        public void ConvertExpression_AlsoOutsideBrackets_Passes()
        {
            string valueToConvert = "[[1d20+150]] vs @{power-1-attack-misc}";
            string result = PowerVisualizer.ConvertRoll20Expression(valueToConvert);
            string[] results = result.Split(' ');
            int integerResult = System.Convert.ToInt32(results[0]);
            Assert.True((integerResult > 0) && (integerResult < 100));
            integerResult = System.Convert.ToInt32(results[2]);
            Assert.True((integerResult > 0) && (integerResult < 100));
        }
    }
}
