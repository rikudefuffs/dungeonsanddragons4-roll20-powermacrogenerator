/* ----------------------------------------------
 * 
 * 				DnD4PowerMacroGenerator
 * 
 * Creation Date: 10/04/2020 17:31:40
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using DnDGenerator;

namespace Tests
{
    public class RitualGeneratorTests
    {
        string originalLanguage;
        [OneTimeSetUp]
        public void Setup()
        {
            originalLanguage = Localization.language;
            Localization.language = "English";
        }

        [OneTimeTearDown]
        public void Teardown()
        {
            Localization.language = originalLanguage;
        }

        [TestCase(1, true, TestName = "Ritual With Roll")]
        [TestCase(1, false, TestName = "Ritual Without Roll")]
        public void RitualGenerator_Passes(int powerIndex, bool addRitualRoll)
        {
            Dictionary<string, object> additionalProperties = new Dictionary<string, object>();

            additionalProperties.Add(GeneratorModel.EMOTE_PROPERTY, "fa un sacco di cose complicate per creare incredibili strumenti magici");
            additionalProperties.Add(GeneratorModel.LEVEL_PROPERTY, "8");

            additionalProperties.Add("type", "Creazione");
            additionalProperties.Add("time", "1 ora");
            additionalProperties.Add("duration", "Istantaneo");
            additionalProperties.Add("cost", "95 gp");
            additionalProperties.Add("ability", "arcana");
            additionalProperties.Add(GeneratorModel.EFFECT_PROPERTY, "Succede qualcosa");
            additionalProperties.Add(GeneratorModel.LINK_PROPERTY, "http://iws.mx/dnd/?view=ritual9");

            string expectedResult =
            "&{template:dnd4epower}{{item=1}}{{emote=@{character_name} fa un sacco di cose complicate per creare incredibili strumenti magici}}" +
            "{{level= @{power-1-level}}}{{name=@{power-1-name}}}{{**Type:**=Creazione}}{{**Time:**=1 ora}}" +
            "{{**Duration:**=Istantaneo}}{{**Cost:**=95 gp}}{{**Ability:**=arcana}}{{**Effect:**=Succede qualcosa}}" +
            "{{**Link to Power (I.E: to wiki):**=[Link to power](http://iws.mx/dnd/?view=ritual9)}}";

            if (addRitualRoll)
            {
                additionalProperties.Add(GeneratorModel.RITUAL_ROLL_PROPERTY, string.Format("{0}|{1}", "arcana", "Gigi"));
                expectedResult += "\n %{Gigi|-arcana-check}";
            }

            Assert.AreEqual(expectedResult, PowerGenerator.GenerateAndReturn(powerIndex, additionalProperties).ToString());
        }
    }
}
