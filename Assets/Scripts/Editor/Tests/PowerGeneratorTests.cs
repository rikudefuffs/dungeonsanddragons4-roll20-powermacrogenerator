using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using DnDGenerator;

namespace Tests
{
    public class PowerGeneratorTests
    {
        string originalLanguage;
        [OneTimeSetUp]
        public void Setup()
        {
            originalLanguage = Localization.language;
            Localization.language = "English";
        }

        [OneTimeTearDown]
        public void Teardown()
        {
            Localization.language = originalLanguage;
        }

        [TestCase(1, PowerUsage.Encounter, new string[] { "Freddo", "Necrotico" }, "Tutti i nemici")]
        public void PowerGeneratorTestsSimplePasses(int powerIndex, PowerUsage powerUsage, string[] keywords, string target)
        {
            Dictionary<string, object> additionalProperties = new Dictionary<string, object>();
            AttackRollData attackRollData = new AttackRollData("AC", true);
            additionalProperties.Add(GeneratorModel.ATTACK_ROLL_PROPERTY, attackRollData);

            DamageRollData damageRollData = new DamageRollData(true, DamageType.Untyped);
            additionalProperties.Add(GeneratorModel.HIT_PROPERTY, damageRollData);

            string expectedResult = "&{template:dnd4epower}{{encounter=1}}{{name=@{power-1-name}}}{{type=@{power-1-useage} ♦}}" +
                "{{keywords=Freddo, Necrotico}}{{action=@{power-1-action} ♦}}{{range=@{power-1-range}}}" +
                "{{target=Tutti i nemici}}{{**Attack:**=[[1d20+@{power-1-attack-misc}]] vs **@{power-1-def}**}}" +
                "{{**Hit:**=[[@{power-1-weapon-num-dice}d@{power-1-weapon-dice}+@{power-1-damage-misc}]] Damage}}";
            Assert.AreEqual(expectedResult, PowerGenerator.Generate(powerIndex, powerUsage, keywords, target, additionalProperties));
        }

        [TestCase("power-1-", true, ExpectedResult = "[[1d20+@{power-1-attack-misc}]] vs **@{power-1-def}**")]
        [TestCase("power-2-", true, ExpectedResult = "[[1d20+@{power-2-attack-misc}]] vs **@{power-2-def}**")]
        [TestCase("power-3-", false, ExpectedResult = "[[1d20+@{power-3-attack}]] vs **@{power-3-def}**")]
        public string GenerateAttackString_Passes(string propertyPrefix, bool miscModifierOnly)
        {
            return PowerGenerator.GenerateAttack(propertyPrefix, miscModifierOnly);
        }

        [TestCase("power-1-", true, DamageType.Untyped, 0, ExpectedResult = "[[@{power-1-weapon-num-dice}d@{power-1-weapon-dice}+@{power-1-damage-misc}]] Damage")]
        [TestCase("power-1-", false, DamageType.Cold, 0, ExpectedResult = "[[@{power-1-weapon-num-dice}d@{power-1-weapon-dice}+@{power-1-damage}]] Cold Damage")]
        [TestCase("power-1-", true, DamageType.Untyped, 10, ExpectedResult = "10 Damage")]
        [TestCase("power-1-", false, DamageType.Fire, 10, ExpectedResult = "10 Fire Damage")]
        public string GenerateHitString_Passes(string propertyPrefix, bool miscModifierOnly, DamageType damageType, int flatDamage)
        {
            DamageRollData damageRollData = new DamageRollData(miscModifierOnly, damageType, flatDamage);
            return PowerGenerator.GenerateHit(propertyPrefix, damageRollData);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator PowerGeneratorTestsWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
