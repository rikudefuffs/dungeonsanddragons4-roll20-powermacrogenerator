using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace com.hololabs.editor
{
    public static class GotoScene
    {
        [MenuItem("Window/Custom/Go to Scene %#k")]
        public static void MenuItem()
        {
            TextInputDialog.Prompt("Scene Switcher", "Scene name, partial ok, case insensitive:", DoGotoScene);
        }

        static void DoGotoScene(string searchTerm)
        {
            if (string.IsNullOrEmpty(searchTerm)) return;
            List<string> matches = AssetDatabase.FindAssets("t:Scene " + searchTerm).Select(AssetDatabase.GUIDToAssetPath).ToList();

            // See if there's an exact match - match full path to search term, case insensitive
            var test = new Regex(string.Format(@"\b{0}.unity$", searchTerm), RegexOptions.Compiled | RegexOptions.IgnoreCase);

            // if there's an exact match use it, otherwise use the first
            string path = matches.Find(test.IsMatch) ?? matches.FirstOrDefault();
            if (path != null) EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
        }
    }
}