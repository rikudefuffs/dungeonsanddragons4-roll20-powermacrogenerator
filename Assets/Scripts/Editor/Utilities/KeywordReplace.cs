﻿/* ----------------------------------------------
 * 
 * 				Athlon Hunters Game
 * 
 * Original Author: Abela paolo
 * Creation Date: 09 April 2016
 * Updates: 
 * 
 * Copyright © 2015-2016 Starwork Game Creators
 * ----------------------------------------------
 */


using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Editor class that replaces the keywords in a Script file with useful data defined by YOU :D.
/// needs to be in an "Editor" folder to work.
/// </summary>

public class KeywordReplace : UnityEditor.AssetModificationProcessor
{
    public static void OnWillCreateAsset(string path)
    {
        //ignore meta files
        path = path.Replace(".meta", "");
        int index = path.LastIndexOf(".");
        if(index > 0)
        {
            //get the extension of the file
            string file = path.Substring(index);

            //if it isn't a script...
            if (file != ".cs" && file != ".js" && file != ".boo")
            {
                return;
            }

            //else proceed
            index = Application.dataPath.LastIndexOf("Assets");
            path = Application.dataPath.Substring(0, index) + path;

            //read the file
            file = System.IO.File.ReadAllText(path);

            //change whatever you want (you can add stuff below, just be sure to add the tags in the template too :D)
            file = file.Replace("#CREATIONDATE#", System.DateTime.Now + "");
            file = file.Replace("#PROJECTNAME#", PlayerSettings.productName);
            //file = file.Replace("#SMARTDEVELOPERS#", PlayerSettings.companyName);

            //Write the changes in the new script
            System.IO.File.WriteAllText(path, file);
            AssetDatabase.Refresh();
        }
    }
}