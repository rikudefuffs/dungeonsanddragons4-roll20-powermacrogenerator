/* ----------------------------------------------
 * 
 *              PacmanHackAdemy
 * 
 * Original Author: Davide Giuseppe Lepore
 * Creation Date: 2/1/2019 11:40:01 AM
 * Updates: 
 * 
 * Copyright � GamingHackAdemy
 * ----------------------------------------------
 */
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.UI;

///<summary>
///
///</summary>
[CustomEditor(typeof(LocalizedDropdown))]
public class LocalizedDropdownEditor : DropdownEditor
{
    SerializedProperty prefix;

    protected override void OnEnable()
    {
        base.OnEnable();
        prefix = serializedObject.FindProperty("prefix");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        EditorGUILayout.PropertyField(prefix);
        serializedObject.ApplyModifiedProperties();
    }
}
